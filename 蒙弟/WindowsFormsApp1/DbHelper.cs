﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    class DbHelper
    {
        private static readonly string conStr = "server=.;database=Schools;uid=sa;pwd=123456;";

        public static DataTable GetDataTable(string sql)
        {
            SqlConnection conn = new SqlConnection(conStr);
            SqlDataAdapter adapter = new SqlDataAdapter(sql, conn);
            conn.Open();
            DataTable dt = new DataTable();
            adapter.Fill(dt);
            return dt;
        }

        public static int EditOrUpdateOrDelete(string sql)
        {
            SqlConnection conn = new SqlConnection(conStr);
            SqlCommand command = new SqlCommand(sql, conn);
            conn.Open();
            return command.ExecuteNonQuery();
        }
    }
}
