use master
go
if exists(select * from SysDataBases where name = 'Schools')
drop database Schools
go
create database Schools
go
use Schools

if exists(select * from SysObjects where name= 'Students')
drop table Students

create table Students
(
	Id int identity(1,1) primary key not null,
	Name nvarchar(80) not null,
	Age int default 20, 
	Score int default 0,
)

insert into Students (Name) values ('蒙弟')

insert into Students (Name) values ('张三')

insert into Students (Name) values ('李四')

insert into Students (Name) values ('王五')

insert into Students (Name) values ('计六')
