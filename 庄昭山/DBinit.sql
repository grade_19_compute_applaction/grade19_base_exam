create database Schools

GO

use Schools

GO

create table Students
(
	Id int primary key identity(1,1) not null,
	Name nvarchar(10) not null,
	Age int  not null,
	Score int default 0 not null,
)

insert into Students (Name,Age,Score) 
values ('王白给',20,85),
	   ('王静香',20,95),
	   ('李水方',21,77),
	   ('姚水珠',19,65),
	   ('张丁悦',20,97),
	   ('黄桂花',19,80),
	   ('蔡旭琨',20,59),
	   ('李白给',19,89),
	   ('肖学乐',19,77),
	   ('释愿为',21,79),
	   ('张东旭',19,91)

