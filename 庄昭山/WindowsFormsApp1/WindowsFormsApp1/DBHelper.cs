﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace WindowsFormsApp1
{
    public class DBHelper
    {
        private static readonly string connStr = "server=.;database=Schools;uid=sa;pwd=123wan520";
        public static  DataTable GetDataTable(string sql)
        {
            SqlConnection connection = new SqlConnection(connStr);

            SqlDataAdapter sqlData = new SqlDataAdapter(sql,connection);

            connection.Open();

            DataTable dt = new DataTable();

            sqlData.Fill(dt);

            return dt;
        }

        public static int AddOrUpdateOrDelete(string sql)
        {
            SqlConnection connection = new SqlConnection(connStr);

            SqlCommand command = new SqlCommand(sql,connection);

            connection.Open();

            var res= command.ExecuteNonQuery();

            return res;

        }
    }
}
