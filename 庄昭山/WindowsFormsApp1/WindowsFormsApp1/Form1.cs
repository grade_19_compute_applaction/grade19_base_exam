﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: 这行代码将数据加载到表“schoolsDataSet.Students”中。您可以根据需要移动或删除它。
            //this.studentsTableAdapter.Fill(this.schoolsDataSet.Students);
            var sql = "select * from Students";

            var dt = DBHelper.GetDataTable(sql);

            dataGridView1.DataSource = dt;
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            dataGridView1.ReadOnly = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var name = textBox1.Text;

            var sql = string.Format("select * from Students where Name like '%{0}%'",name);
            var dt = DBHelper.GetDataTable(sql);
            dataGridView1.DataSource = dt;
            var pd = dataGridView1.Rows.Count;
            if (pd==0)
            {
                MessageBox.Show("未找到该学生信息！", "提示！", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        //添加
        private void button2_Click(object sender, EventArgs e)
        {
            AddForm addForm = new AddForm();
            var res = addForm.ShowDialog();

            if (res==DialogResult.OK)
            {
                var sql = "select * from Students";
                var dt = DBHelper.GetDataTable(sql);
                dataGridView1.DataSource = dt;
            }
            else
            {
                MessageBox.Show("NO！","提示：",MessageBoxButtons.OK,MessageBoxIcon.Information);
            }
        }
        //更新
        private void button3_Click(object sender, EventArgs e)
        {
            var row = dataGridView1.SelectedRows[0];
            var id = (int)row.Cells[0].Value;
            var name = (string)row.Cells[1].Value;
            var age = (int)row.Cells[2].Value;
            var score = (int)row.Cells[3].Value;

            AddForm addForm = new AddForm(id,name,age,score);
            var res = addForm.ShowDialog();

            if (res == DialogResult.OK)
            {
                var sql = "select * from Students";
                var dt = DBHelper.GetDataTable(sql);
                dataGridView1.DataSource = dt;
            }
            else
            {
                MessageBox.Show("NO！","提示！",MessageBoxButtons.OK,MessageBoxIcon.Information);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            var id = dataGridView1.SelectedRows[0].Cells[0].Value;

            var sql = string.Format("delete from Students where Id={0}",id);

            var res = DBHelper.AddOrUpdateOrDelete(sql);
            if (res == 1)
            {
                MessageBox.Show("删除成功！","提示！",MessageBoxButtons.OK,MessageBoxIcon.Information);
                var sqlselect = "select * from Students";
                var dt = DBHelper.GetDataTable(sqlselect);
                dataGridView1.DataSource = dt;
            }
            else
            {
                MessageBox.Show("删除失败！","提示！",MessageBoxButtons.OK,MessageBoxIcon.Information);

            }
        }
    }
}
