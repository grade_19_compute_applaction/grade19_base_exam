﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private int id;

        public Form2(int id, string name, int age, int score)
        {
            //初始化
            InitializeComponent();
            this.id = id;
            //获取姓名
            textBox1.Text = name;
            //获取年龄
            textBox2.Text = age.ToString();
            //获取成绩
            textBox3.Text = score.ToString();
        }

        //信息确认保存
        private void button1_Click(object sender, EventArgs e)
        {
            var name = textBox1.Text;
            var age = textBox2.Text;
            var score = textBox3.Text;


            if (this.id > 0)
            {
                //信息更新
                var sql = string.Format("update Students set Name='{0}',Age={1},Score={2} where Id={3}", name, age, score, this.id);

               Class1.AddOrUpdateOrDelete(sql);

                MessageBox.Show("更新成功！", "提示");

            }

            //信息添加
            else
            {

                var sql = string.Format("insert into Students (Name,Age,Score) values ('{0}',{1},{2})", name, age, score);

                Class1.AddOrUpdateOrDelete(sql);

                MessageBox.Show("添加成功！", "提示");


            }
            this.DialogResult = DialogResult.Yes;

            this.Close();
        }
        //关闭窗口
        private void button2_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }
    }
}
