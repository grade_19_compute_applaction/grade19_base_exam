﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        //查询信息
        private void button1_Click(object sender, EventArgs e)
        {
            var name = textBox1.Text;
            //查询
            var sql = string.Format("select * from Students where Name like '%{0}%'", name);

            var sdt = Class1.GetDataTable(sql);

            dataGridView1.DataSource = sdt;
        }
        //添加信息
        private void button2_Click(object sender, EventArgs e)
        {
            Form2 form2 = new Form2();
            var ig = form2.ShowDialog();
            if (ig == DialogResult.Yes)
            {
                //实时查询刷新
                var qwe = "select * from Students";

                var sdt = Class1.GetDataTable(qwe);

                dataGridView1.DataSource = sdt;

            }
            else
            {
                MessageBox.Show("添加失败!", "提示");
            }

        }
        //修改信息
        private void button3_Click(object sender, EventArgs e)
        {
            var id = (int)dataGridView1.SelectedRows[0].Cells[0].Value;
            var name = (string)dataGridView1.SelectedRows[0].Cells[1].Value;
            var age = (int)dataGridView1.SelectedRows[0].Cells[2].Value;
            var score = (int)dataGridView1.SelectedRows[0].Cells[3].Value;

            Form2 form2 = new Form2(id, name, age, score);
            var rng = form2.ShowDialog();

            if (rng == DialogResult.Yes)
            {
                //实时查询刷新
                var qwe = "select * from Students";

                var sdt = Class1.GetDataTable(qwe);

                dataGridView1.DataSource = sdt;

                //MessageBox.Show("修改成功！", "提示");
            }
            else
            {
                MessageBox.Show("修改失败!", "提示");
            }

        }
        //删除信息
        private void button4_Click(object sender, EventArgs e)
        {
            var row = dataGridView1.SelectedRows[0];

            var cell = row.Cells[0];

            var id = (int)dataGridView1.SelectedRows[0].Cells[0].Value;

            var sql = string.Format("delete from Students where Id={0}", id);

            var fpx = Class1.AddOrUpdateOrDelete(sql);
            if (fpx == 1)
            {
                //实时查询刷新
                var qwe = "select * from Students";

                var sdt = Class1.GetDataTable(qwe);

                dataGridView1.DataSource = sdt;

                MessageBox.Show("删除成功！", "提示");
            }
            else
            {
                MessageBox.Show("删除失败！", "提示");
            }
        }
        //关闭窗口
        private void button5_Click(object sender, EventArgs e)
        {
            MessageBox.Show("关闭成功！", "提示");
            this.Close();
        }
        //load加载窗体
        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: 这行代码将数据加载到表“schoolsDataSet.Students”中。您可以根据需要移动或删除它。
            //this.studentsTableAdapter.Fill(this.schoolsDataSet.Students);
            var qwe = "select * from Students";

            var sdt =Class1.GetDataTable(qwe);

            dataGridView1.DataSource = sdt;
            //单击选中一整行
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            //不允许编辑
            dataGridView1.ReadOnly = true;
            //不显示下一个空白行
            dataGridView1.AllowUserToAddRows = false;
        }
    }
}
