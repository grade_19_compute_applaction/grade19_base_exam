﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    class Class1
    {
        //连接SQL数据库
        private static readonly string conString = "server=.;database=Schools;uid=sa;pwd=123456;";
        public static DataTable GetDataTable(string sql)
        {
            
            SqlConnection connection = new SqlConnection(conString);

            SqlDataAdapter adapter = new SqlDataAdapter(sql, connection);
            //打开连接
            connection.Open();

            DataTable sdt = new DataTable();

            adapter.Fill(sdt);
            //返回值
            return sdt;
        }

        public static int AddOrUpdateOrDelete(string sql)
        {
            SqlConnection connection = new SqlConnection(conString);

            SqlCommand command = new SqlCommand(sql, connection);

            connection.Open();

            return command.ExecuteNonQuery();
        }

    }
}

