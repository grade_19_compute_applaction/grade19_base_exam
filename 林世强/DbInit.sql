create database Schools
go

use Schools
go
create table Students
(
Id int primary key identity,
Name nvarchar(50) not null,
Age int not null,
Score int not null,
)
go

insert into Students (Name,Age,Score) values ('小林',18,78)
insert into Students (Name,Age,Score) values ('小王',20,89)
insert into Students (Name,Age,Score) values ('小李',19,74)
insert into Students (Name,Age,Score) values ('小赵',18,83)
go

select*from Students