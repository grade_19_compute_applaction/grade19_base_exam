use master
if exists (select * from sys.databases where name = 'Schools')
drop database Work
go

create database Schools
on
(
name='Schools_date',
filename='F:\SQL server\Schools_date.mdf',
size=5mb,
maxsize=100mb,
filegrowth=15%
)
log on 
(
name='Schools_log',
filename='F:\SQL server\Schools_log.ldf',
size=2mb,
maxsize=50mb,
filegrowth=1mb
)


use Schools
go
if exists (select * from sys.objects where name = 'Students')
drop table Students
go

create table Students
(
	Id int primary key identity(1,1),
	Name nvarchar(50) not null,
	Age int default 18 not null,
	Sex nvarchar(1) check (Sex in('��','Ů')),
	Score int default 85 not null
)
go


insert into Students(Name,Age,Sex,Score) values('����',19,'Ů',default)
insert into Students(Name,Age,Sex,Score) values('�Ϲ���',21,'Ů',default)
insert into Students(Name,Age,Sex,Score) values('����',default,'Ů',default)
insert into Students(Name,Age,Sex,Score) values('˾����',23,'��',default)
insert into Students(Name,Age,Sex,Score) values('����',19,'��',default)
insert into Students(Name,Age,Sex,Score) values('������',21,'��',default)
insert into Students(Name,Age,Sex,Score) values('����',default,'Ů',default)
insert into Students(Name,Age,Sex,Score) values('�Ի�',19,'Ů',default)
go

insert into Students(Name,Age,Sex,Score) values('����',10,'��',default)
insert into Students(Name,Age,Sex,Score) values('������',default,'��',default)
insert into Students(Name,Age,Sex,Score) values('����ǭ',19,'Ů',default)
insert into Students(Name,Age,Sex,Score) values('����',20,'Ů',default)
insert into Students(Name,Age,Sex,Score) values('����',19,'��',default)
go

select * from Students

select * from Students where Name like '%��%'

insert into Students(Name,Age,Sex,Score) values('�־�',19,'Ů',88)
go

update Students set Name = '������',Age = 21,Sex = '��',Score = default where Id = 14

delete from Students where Id = 14

