﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            var Sch = "select * from Students";

            var dt = DBHelper.GetDataTable(Sch);

            dataGridView1.DataSource = dt;

            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView1.ReadOnly = true;
            dataGridView1.AllowUserToAddRows = false;
        }

        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            var name = textBox1.Text.Trim();
            var query = string.Format("select * from Students where Name like '%{0}%'", name);

            var dt = DBHelper.GetDataTable(query);

            dataGridView1.DataSource = dt;
        }

        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            EditForm ef = new EditForm();
            var res = ef.ShowDialog();

            if (res == DialogResult.Yes)
            {
                var Sch = "select * from Students";

                var dt = DBHelper.GetDataTable(Sch);

                dataGridView1.DataSource = dt;
            }
            else
            {
                MessageBox.Show("很遗憾，添加失败！", "消息提示框");
            }
        }

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                var id = (int)dataGridView1.SelectedRows[0].Cells["Id"].Value;
                var name = (string)dataGridView1.SelectedRows[0].Cells["Name"].Value;
                var age = (int)dataGridView1.SelectedRows[0].Cells["Age"].Value;
                var sex = (string)dataGridView1.SelectedRows[0].Cells["Sex"].Value;
                var score = (int)dataGridView1.SelectedRows[0].Cells["Score"].Value;

                EditForm ef = new EditForm(id,name,age,sex,score);
                var res = ef.ShowDialog();

                if (res == DialogResult.Yes)
                {
                    var tempname = textBox1.Text.Trim();
                    var sqlquery = string.Format("select * from Students where Name like '%{0}%'", tempname);

                    var dt = DBHelper.GetDataTable(sqlquery);

                    dataGridView1.DataSource = dt;
                }
                else
                {
                    MessageBox.Show("很遗憾，更新失败！","消息提示框");
                }
            }
            else
            {
                MessageBox.Show("当前您未选择需要更新的数据！", "消息提示框");
            }
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button4_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                var id = (int)dataGridView1.SelectedRows[0].Cells["Id"].Value;
                var delete = string.Format("delete from Students where Id = {0}", id);

                var res = DBHelper.AddOrUpdateOrDelete(delete);

                if (res == 1)
                {
                    MessageBox.Show("恭喜您，删除成功！", "消息提示框");

                    var name = textBox1.Text.Trim();
                    var sqlquery = string.Format("select * from Students where Name like '%{0}%'", name);

                    var dt = DBHelper.GetDataTable(sqlquery);

                    dataGridView1.DataSource = dt;
                }
                else
                {
                    MessageBox.Show("很遗憾，删除失败！", "消息提示框");
                }
            }
            else
            {
                MessageBox.Show("当前您未选择需要删除的数据！", "消息提示框");
            }
        }
    }
}
