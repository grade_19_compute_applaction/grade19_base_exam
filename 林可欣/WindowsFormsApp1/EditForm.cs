﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class EditForm : Form
    {
        public EditForm()
        {
            InitializeComponent();
        }

        private int Id;

        public EditForm(int id, string name, int age, string sex, int score)
        {
            InitializeComponent();

            this.Id = id;
            textBox1.Text = name;
            textBox2.Text = age.ToString();
            textBox3.Text = sex;
            textBox4.Text = score.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var name = textBox1.Text;
            var age = textBox2.Text;
            var sex = textBox3.Text;
            var score = textBox4.Text;

            //更新
            if (this.Id > 0)
            {
                var replace = string.Format("update Students set Name = '{0}',Age = {1},Sex = '{2}',Score = {3} where Id = {4}", name, age, sex, score, this.Id);

                DBHelper.AddOrUpdateOrDelete(replace);

                MessageBox.Show("恭喜您，更新成功！", "消息提示框");
            }
            //添加
            else
            {
                var add = string.Format("insert into Students(Name,Age,Sex,Score) values('{0}',{1},'{2}',{3})", name, age, sex, score);

                DBHelper.AddOrUpdateOrDelete(add);

                MessageBox.Show("恭喜您，添加成功！", "消息提示框");
            }

            this.DialogResult = DialogResult.Yes;
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }
    }
}
