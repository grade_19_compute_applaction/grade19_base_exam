﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    public class DBHelper
    {
        private static string Constr = "server=.;database=Schools;uid=sa;pwd=123456";

        public static DataTable GetDataTable(string sql)
        {
            SqlConnection conn = new SqlConnection(Constr);

            SqlDataAdapter adap = new SqlDataAdapter(sql,conn);

            conn.Open();

            DataTable dt = new DataTable();

            adap.Fill(dt);

            return dt;
        }

        public static int AddOrUpdateOrDelete(string sql)
        {
            SqlConnection conn = new SqlConnection(Constr);

            SqlCommand comm = new SqlCommand(sql, conn);

            conn.Open();

            return comm.ExecuteNonQuery();
        }
    }
}
