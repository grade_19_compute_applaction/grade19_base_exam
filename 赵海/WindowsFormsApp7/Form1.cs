﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp7
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            var sql = "select * from Students"; 
            var dt = zhao.GetDataTable(sql);
            dataGridView1.DataSource = dt;

            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView1.ReadOnly = true;
            dataGridView1.AllowUserToAddRows = false;
        }
        //查询
        private void button4_Click(object sender, EventArgs e)
        {
            var name = textBox1.Text.Trim();
            var sql = string.Format("select * from Students where StudentName like '%{0}%'",name);
            var dt = zhao.GetDataTable(sql);

            dataGridView1.DataSource = dt;

            
        }
        //增加
        private void button1_Click(object sender, EventArgs e)
        {  
            Form2 form = new Form2();
            var res=form.ShowDialog();
            if (res == DialogResult.Yes)
            {
                var sql = "select * from Students";
                var dt = zhao.GetDataTable(sql);
                dataGridView1.DataSource = dt;
            }
            else
            {
                MessageBox.Show("No");
            }
        }
        //修改
        private void button2_Click(object sender, EventArgs e)
        {
            var id = (int)dataGridView1.SelectedRows[0].Cells["Id"].Value;
            var name = (string )dataGridView1.SelectedRows[0].Cells["StudentName"].Value;
            var age = (int)dataGridView1.SelectedRows[0].Cells["Age"].Value;
            var score = (int)dataGridView1.SelectedRows[0].Cells["Score"].Value;
            Form2 form = new Form2(id,name,age,score);
            //var res = form.ShowDialog();
            //if (res == DialogResult.Yes)
            //{
            //    var stuSql = "select * from students";
            //    var dt = zhao.GetDataTable(stuSql);
            //    dataGridView1.DataSource = dt;
            //}
            //else 
            //{
            //    MessageBox.Show("No");
            //}
            var res=form.ShowDialog();
            if (res == DialogResult.Yes)
            {
                var sql = "select * from Students";
                var dt = zhao.GetDataTable(sql);
                dataGridView1.DataSource = dt;
            }
            else 
            {
                MessageBox.Show("No");
            }
        }
        //删除
        private void button3_Click(object sender, EventArgs e)
        {
            var rows = dataGridView1.SelectedRows;
            if (rows.Count > 0)
            {
                var id = (int)rows[0].Cells["Id"].Value;
                var sql = string.Format("delete from Students where id={0}", id);
                var resCount = zhao.AddOrUpdateOrDelete(sql);

                if (resCount > 0)
                {
                    MessageBox.Show("删除成功", "提示");
                }
                else
                {
                    MessageBox.Show("删除失败", "提示");

                }
            }
            else 
            {
                MessageBox.Show("选择要删除的行","提示");
            }
            
        }
    }
}
