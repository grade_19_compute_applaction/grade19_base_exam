use master
go
if exists (select * from sysDataBases where name = 'Schools')
drop database Schools
go 

--创建数据库
create database Schools
go
use Schools 
go
--创建数据表
create table Students
(
 Id int primary key identity (1,1),
 StudentName nvarchar(20),
 Age int not null,
 Score int not null,
)
go
insert into Students (StudentName,Age,Score) values ('刘宇','18','100')
insert into Students (StudentName,Age,Score) values ('赵刚','20','80')
insert into Students (StudentName,Age,Score) values ('王奕程','18','70')
insert into Students (StudentName,Age,Score) values ('宋晓云','19','90')
insert into Students (StudentName,Age,Score) values ('如花','18','75')
insert into Students (StudentName,Age,Score) values ('翠花','19','85')
insert into Students (StudentName,Age,Score) values ('孙小龙','18','60')
go
