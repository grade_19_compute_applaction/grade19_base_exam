﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private int id;


        public Form2(int id, string name, int age, int score)
        {
            InitializeComponent();
            this.id = id;
            textBox1.Text = name;
            textBox2.Text = age.ToString();
            textBox3.Text = score.ToString();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var name = textBox1.Text;
            var age = textBox2.Text;
            var score = textBox3.Text;

            //更新
            if (this.id > 0)
            {

                var sql = string.Format("update  Students set StudentName ='{0}',Age ={1},Score ={2}  Where Id={3}", name, age, score, this.id);

                DbHelper.AddOrUpdateOrDelete(sql);

                MessageBox.Show("更新成功！", "提示");
            }
            //添加
            else
            {
                var sql = string.Format("insert into Students (StudentName,Age,Score) values('{0}',{1},{2})", name, age, score);
                DbHelper.AddOrUpdateOrDelete(sql);

                MessageBox.Show("添加成功！", "提示");
            }

            this.DialogResult = DialogResult.Yes;

            this.Close();

        }

        private void Form2_Load(object sender, EventArgs e)
        {

        }
    }
}
