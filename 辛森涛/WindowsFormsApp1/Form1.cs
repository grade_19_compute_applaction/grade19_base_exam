﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: 这行代码将数据加载到表“schoolsDataSet.Students”中。您可以根据需要移动或删除它。
            this.studentsTableAdapter1.Fill(this.schoolsDataSet.Students);

            // TODO: 这行代码将数据加载到表“mySchoolDataSet.student”中。您可以根据需要移动或删除它。

            var abc = "select * from Students";//abc就是一个变量名

            var stuSql = "select * from Students";
            
            var dt = DbHelper.GetDataTable(stuSql);

            dataGridView1.DataSource = dt;  //赋值给数据源

            //限制dataGridView1的某些行为  
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;//一次选择一行
            dataGridView1.ReadOnly = true;  //只可阅读，不可编辑
            dataGridView1.AllowUserToAddRows = false;  //不显示新一行列
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button5_Click(object sender, EventArgs e)
        {
            var row = dataGridView1.SelectedRows[0];

            var cell = row.Cells[0];
            
            var id = (int)dataGridView1.SelectedRows[0].Cells[0].Value;

            var sql = string.Format("delete from Students where Id={0}",id);

            var res = DbHelper.AddOrUpdateOrDelete(sql);

            if (res == 1)
            {
                MessageBox.Show("删除成功！", "提示");

                var stuSql = "select * from Students";

                var dt = DbHelper.GetDataTable(stuSql);

                dataGridView1.DataSource = dt;
            }
            else
            {
                MessageBox.Show("删除失败", "提示");
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            var name = textBox1.Text;
            var sql = string.Format("select * from Students where Name like '%{0}%'",name);

            var dt = DbHelper.GetDataTable(sql);

            dataGridView1.DataSource = dt;
        }

        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)  
        {
            Form2 form = new Form2();
            var res=form.ShowDialog();
            if (res == DialogResult.Yes)
            {
                var stuSql = "select * from Students";

                var dt = DbHelper.GetDataTable(stuSql);

                dataGridView1.DataSource = dt;
            }
            else
            {
                MessageBox.Show("No");
            }
        }

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button4_Click(object sender, EventArgs e)
        {
            var id = (int)dataGridView1.SelectedRows[0].Cells[0].Value;
            var name = (string)dataGridView1.SelectedRows[0].Cells[1].Value;
            var age = (int)dataGridView1.SelectedRows[0].Cells[2].Value;
            var score = (int)dataGridView1.SelectedRows[0].Cells[3].Value;
            Form2 form = new Form2(id,name,age,score);
            var res=form.ShowDialog();
            if (res == DialogResult.Yes)
            {
                var stuSql = "select * from Students";

                var dt = DbHelper.GetDataTable(stuSql);

                dataGridView1.DataSource = dt;
            }
            else
            {
                MessageBox.Show("No");
            }
        }
    }
}
