use master

GO

IF EXISTS(SELECT * FROM SysDatabAses WHERE Name ='Schools')
DROP DATABASE Schools

GO

CREATE DATABASE Schools
ON PRIMARY
(
	NAME ='Schools',
	FILENAME ='D:\Schools.mdf',
	SIZE=5MB,
	FILEGROWTH=1MB

)
LOG ON
(
	NAME='Schools_log',
	FILENAME='D:\Schools_log.ldf',
	SIZE=1MB,
	FILEGROWTH=10%

)
GO
use Schools
go
IF EXISTS(SELECT * FROM SysObjects where name ='Students')
DROP TABLE Students
CREATE TABLE Students
(
	Id int primary key identity(1,1),
	Name nvarchar(80) not null,
	Age int not null,
	Score int not null

)
go

insert into Students (Name,Age,Score) values('',20,'')
select * from Students

insert into Students (Name,Age,Score) values('�','18','80')