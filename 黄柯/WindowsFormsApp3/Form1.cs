﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace WindowsFormsApp2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        //添加
        private void button2_Click(object sender, EventArgs e)
        {
            Form2 form2 = new Form2();
            var res = form2.ShowDialog();
           
            if (res == DialogResult.Yes)
            {
                var select = "select * from Students";

                var dt = Dbhelper.GetDataTable(select);

                dataGridView1.DataSource = dt;
            }
            else
            {
                MessageBox.Show("添加失败！", "提示");
            }
        }

        //删除
        private void button4_Click(object sender, EventArgs e)
        {
            var row = dataGridView1.SelectedRows[0];

            var id = (int)dataGridView1.SelectedRows[0].Cells[0].Value;

            var sql = string.Format("delete from Students where Id={0}", id);

            var sure = MessageBox.Show("确定删除？","提示!");

            if (sure == DialogResult.OK)
            {
                Dbhelper.AddOrUpdate(sql);
                MessageBox.Show("删除成功");

                var select = "select * from Students";
                var dt = Dbhelper.GetDataTable(select);
                dataGridView1.DataSource = dt;
            }
            else
            {
                MessageBox.Show("删除失败");
            }

        }

        //修改
        private void button5_Click(object sender, EventArgs e)
        {
            var id = (int)dataGridView1.SelectedRows[0].Cells[0].Value;
            var name = (string)dataGridView1.SelectedRows[0].Cells[1].Value;
            var age = (int)dataGridView1.SelectedRows[0].Cells[2].Value;
            var score = (int)dataGridView1.SelectedRows[0].Cells[3].Value;

            Form2 form2 = new Form2(id, name, age, score);
            var res = form2.ShowDialog();

            if (res == DialogResult.Yes)
            {
                var select = "select * from Students";

                var dt = Dbhelper.GetDataTable(select);

                dataGridView1.DataSource = dt;
            }
            else
            {
                MessageBox.Show("更新失败","提示");
            }
        }

        //页面查询
        private void Form1_Load(object sender, EventArgs e)
        {
            var select = "select * from Students";

            var dt = Dbhelper.GetDataTable(select);

            dataGridView1.DataSource = dt;

            //单击选中一整行
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            //不允许编辑
            dataGridView1.ReadOnly = true;
            //不显示下一个空白行
            dataGridView1.AllowUserToAddRows = false;
        }


        //button1查询
        private void button1_Click_1(object sender, EventArgs e)
        {
            var name = textBox1.Text;
            var find = string.Format("select * from Students where Name like '%{0}%'", name);

            var dt = Dbhelper.GetDataTable(find);
            
            dataGridView1.DataSource = dt;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}