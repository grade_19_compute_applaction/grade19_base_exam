CREATE DATABASE Schools
GO
USE Schools
GO
CREATE TABLE Students
(
    Id INT IDENTITY(1,1) PRIMARY KEY,
	Name NVARCHAR(20) NOT NULL,
	Age INT NOT NULL,
	Score  INT CHECK(Score BETWEEN 0 AND 750)NOT NULL,
)
   insert into  Students(Name,Age ,Score)    values('王月馨',20,500)
   insert into  Students(Name,Age ,Score)    values('张三',23,343)
   insert into  Students(Name,Age ,Score)    values('小王',26,566)
   insert into  Students(Name,Age ,Score)    values('张林',23,455)
   insert into  Students(Name,Age ,Score)    values('黄灿',23,634)
   insert into  Students(Name,Age ,Score)    values('李伟',23,344)
   insert into  Students(Name,Age ,Score)    values('夏馨',23,022)
   insert into  Students(Name,Age ,Score)    values('李涵月',23,200)
   insert into  Students(Name,Age ,Score)    values('李书',23,342)
   insert into  Students(Name,Age ,Score)    values('韩涵涵',24,342)
   insert into  Students(Name,Age ,Score)    values('李四',25,345)
   insert into  Students(Name,Age ,Score)    values('简语',26,700)

     select * from Students
	 select * from Students where Name like '%涵%'