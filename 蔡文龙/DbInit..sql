---建库
create database Schools
go
use Schools
go
--表
create table Students(
	Id int primary key identity(1,1) not null,
	Name nvarchar(30) not null,
	Age int not null,
	Score int not null
)
go
select * from Students

insert into Students(Name,Age,Score) values
('小黑',18,100),
('小白',18,82),
('小黄',18,80),
('小山',20,30),
('小猪',19,88),
('小紫',19,91)
update Students set Name = '小黑',Age = 20 ,Score =50 where Id =2

