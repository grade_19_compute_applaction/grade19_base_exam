﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    public class DbHelper
    {
        private static readonly string connStr = "server=.;database=Schools;uid=sa;pwd=123456";
        public static DataTable GetDataTable(string sql)
        {
            SqlConnection sqlConn = new SqlConnection(connStr);
            SqlDataAdapter sqlAdapter = new SqlDataAdapter(sql, sqlConn);
            sqlConn.Open();
            DataTable dt = new DataTable();
            sqlAdapter.Fill(dt);
            return dt;
        }
        public static int AddOrUpdateOrDelete(string sql)
        {
            SqlConnection sqlConn = new SqlConnection(connStr);
            SqlCommand sqlComm = new SqlCommand(sql, sqlConn);
            sqlConn.Open();
            var res = sqlComm.ExecuteNonQuery();
            return res;

        }
    }
}
