﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Exam
{
    public partial class EditData : Form
    {
        public EditData()
        {
            InitializeComponent();
        }
        public int stuId;
        public EditData(int stuId , string stuName , int stuAge , string stuSex , int score)
        {
            InitializeComponent();
            this.stuId = stuId;
            this.stuName.Text = stuName;
            this.stuAge.Text = stuAge.ToString();
            this.stuSex.Text = stuSex;
            this.score.Text = score.ToString();
        }
        //弹窗
        public static MessageBoxButtons button_OK = MessageBoxButtons.OK;
        public static MessageBoxButtons button_YesOrNo = MessageBoxButtons.YesNo;
        public static MessageBoxIcon icon_Information = MessageBoxIcon.Information;
        public static MessageBoxIcon icon_Error = MessageBoxIcon.Error;

        //加载
        private void EditData_Load(object sender, EventArgs e)
        {
            //将下拉框的第一项作为默认值
            this.stuSex.Text = (string)this.stuSex.Items[0];
            //判断正在进行的是更新还是添加
            if (this.stuId > 0)
            {
                //更新
                this.Text = "更新学生数据";
            }
            else
            {
                //添加
                this.Text = "添加学生";
            }
        }
        //保存
        private void save_Click(object sender, EventArgs e)
        {
            //利用是否具有stuId判断是添加还是更新
            if (this.stuId > 0)
            {
                //更新
                string update =
                        "UPDATE Students " +
                        "SET " +
                            "stuName = '"+this.stuName.Text+"', " +
                            "stuAge = " + this.stuAge.Text + ", " +
                            "stuSex = '"+this.stuSex.Text+"' " +
                        "WHERE stuId = " + this.stuId;
                int i = DBHelper.ExecuteNonQuery(update);
                if (i > 0)
                {
                    //更新成功
                    MessageBox.Show("更新成功！","提示",button_OK,icon_Information);
                }
                else
                {
                    //更新失败
                    MessageBox.Show("更新失败！", "提示", button_OK, icon_Error);
                }
                this.DialogResult = DialogResult.Yes;
                this.Close();
            }
            else
            {
                //添加
                if (this.stuName.Text != "" && this.stuAge.Text != "" && this.score.Text == "")
                {
                    string add =
                        "INSERT INTO Students(stuName,stuAge,stuSex,score) " +
                        "VALUES('" + this.stuName.Text + "' , " + this.stuAge.Text + " , '" + this.stuSex.Text + "' , '"+this.score.Text+"')";
                    int i = DBHelper.ExecuteNonQuery(add);
                    if (i > 0)
                    {
                        //添加成功
                        MessageBox.Show("添加成功！", "提示", button_OK, icon_Information);
                    }
                    else
                    {
                        //添加失败
                        MessageBox.Show("添加失败！", "提示", button_OK, icon_Error);
                    }
                    this.DialogResult = DialogResult.Yes;
                    this.Close();
                }
                else if (this.stuName.Text == "")
                {
                    MessageBox.Show("学生姓名不能为空！","提示",button_OK,icon_Error);
                }
                else if (this.stuAge.Text == "")
                {
                    MessageBox.Show("学生年龄不能为空！","提示",button_OK,icon_Error);
                }
                else if (this.score.Text == "")
                {
                    MessageBox.Show("成绩不能为空！","提示",button_OK,icon_Error);
                }
            }
        }
        //取消
        private void cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }

    }
}
