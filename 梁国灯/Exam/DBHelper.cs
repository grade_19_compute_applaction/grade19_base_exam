﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Exam
{
    static class DBHelper
    {
        //连接
        private static SqlConnection GetConnection()
        {
            //连接字符串
            string connStr = "server = . ; database = Schools ; uid = sa ; pwd = 193746";
            //声明连接对象
            SqlConnection conn = new SqlConnection(connStr);
            return conn;
        }
        //查询
        public static DataTable QueryByGetDataTable(string query)
        {
            //获得连接
            SqlConnection conn = GetConnection();
            //声明适配器对象
            SqlDataAdapter sda = new SqlDataAdapter(query, conn);
            //声明数据表对象
            DataTable dt = new DataTable();
            //打开连接
            conn.Open();
            //将查询到的数据填充至dt
            sda.Fill(dt);
            //断开连接
            conn.Close();
            return dt;
        }
        //增删改
        public static int ExecuteNonQuery(string update)
        {
            //获得连接
            SqlConnection conn = GetConnection();
            //声明指令对象
            SqlCommand cmd = new SqlCommand(update, conn);
            //打开连接
            conn.Open();
            //执行(获取返回行数)
            int i = cmd.ExecuteNonQuery();
            return i;
        }
    }
}
