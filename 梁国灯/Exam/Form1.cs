﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Exam
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        //弹窗
        public static MessageBoxButtons button_OK = MessageBoxButtons.OK;
        public static MessageBoxButtons button_YesOrNo = MessageBoxButtons.YesNo;
        public static MessageBoxIcon icon_Information = MessageBoxIcon.Information;
        public static MessageBoxIcon icon_Error = MessageBoxIcon.Error;

        //加载
        private void Form1_Load(object sender, EventArgs e)
        {
            //填充数据
            string allData = "SELECT * FROM Students";
            DataTable dt = DBHelper.QueryByGetDataTable(allData);
            this.dataGridView1.DataSource = dt;

            //设置数据视图
            this.dataGridView1.AllowUserToAddRows = false; //不显示添加行
            this.dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect; //默认选择整行数据
            this.dataGridView1.ReadOnly = true; //只读，无法编辑数据
        }
        //查找
        private void query_Click(object sender, EventArgs e)
        {
            string queryBystuName = "SELECT * FROM Students WHERE stuName LIKE '%"+this.stuName.Text+"%'";
            DataTable Students = DBHelper.QueryByGetDataTable(queryBystuName);
            this.dataGridView1.DataSource = Students;
        }
        //添加
        private void add_Click(object sender, EventArgs e)
        {
            EditData add = new EditData();
            add.ShowDialog();
            if (add.DialogResult == DialogResult.Yes)
            {
                //刷新数据
                string allData = "SELECT * FROM Students";
                DataTable dt = DBHelper.QueryByGetDataTable(allData);
                this.dataGridView1.DataSource = dt;
            }
            

        }
        //更新
        private void update_Click(object sender, EventArgs e)
        {
            int selectCount = this.dataGridView1.SelectedRows.Count; //获取当前选中的数据行数
            if (selectCount > 0)
            {
                //将所选中的行数据取出(多选则取第一个)
                int stuId = (int)this.dataGridView1.SelectedRows[0].Cells["stuId"].Value;
                string stuName = (string)this.dataGridView1.SelectedRows[0].Cells["stuName"].Value;
                int stuAge = (int)this.dataGridView1.SelectedRows[0].Cells["stuAge"].Value;
                string stuSex = (string)this.dataGridView1.SelectedRows[0].Cells["stuSex"].Value;
                int score = (int)this.dataGridView1.SelectedRows[0].Cells["score"].Value;
                EditData update = new EditData(stuId,stuName,stuAge,stuSex,score);
                update.ShowDialog();
                //对话框结果
                if (update.DialogResult == DialogResult.Yes)
                {
                    //刷新数据
                    string allData = "SELECT * FROM Students";
                    DataTable dt = DBHelper.QueryByGetDataTable(allData);
                    this.dataGridView1.DataSource = dt;
                }
            }
            else
            {
                MessageBox.Show("你还没有选中要更新的数据行！","提示",button_OK,icon_Information);
            }
        }
        //删除
        private void delete_Click(object sender, EventArgs e)
        {
            DialogResult isDelete = MessageBox.Show("确定要删除吗？删除后此数据将无法恢复！","提示",button_YesOrNo,icon_Information);
            if (isDelete == DialogResult.Yes)
            {
                int stuId = (int)this.dataGridView1.SelectedRows[0].Cells["stuId"].Value;
                string delete =
                            "DELETE FROM Students " +
                            "WHERE stuId = " + stuId;
                int i = DBHelper.ExecuteNonQuery(delete);
                if (i > 0)
                {
                    //删除成功
                    MessageBox.Show("删除成功！", "提示", button_OK, icon_Information);
                }
                else
                {
                    //删除失败
                    MessageBox.Show("删除失败！", "提示", button_OK, icon_Error);
                }
                //刷新数据
                string allData = "SELECT * FROM Students";
                DataTable dt = DBHelper.QueryByGetDataTable(allData);
                this.dataGridView1.DataSource = dt;
            }
            else
            {
                MessageBox.Show("你取消了删除！","提示",button_OK,icon_Information);
            }
        }
    }
}
