USE master
GO
--创建数据库
IF EXISTS(SELECT * FROM SysDataBases WHERE NAME = 'Schools')
DROP DATABASE Schools
GO
CREATE DATABASE Schools
GO
USE Schools
GO
--创建数据表
IF EXISTS(SELECT * FROM SysObjects WHERE NAME = 'Students')
DROP TABLE Students
GO
CREATE TABLE Students
(
	stuId INT IDENTITY(1,1) PRIMARY KEY,
	stuName NVARCHAR(10) NOT NULL,
	stuAge INT CHECK(stuAge BETWEEN 15 AND 30),
	stuSex NVARCHAR(1) CHECK(stuSex = '男' OR stuSex = '女') DEFAULT('女'),
	score INT CHECK(score BETWEEN 0 AND 100)
)
GO
--插入数据
INSERT INTO Students(stuName , stuAge , stuSex , score)
VALUES
	('Tom' , 16 , '男' , 65) , 
	('Jerry' , 17 , '男' , 70) , 
	('张晓红' , 20 , '女' , 59) ,
	('王海' , 23 , '男' , 99) ,
	('李芳' , 25 , '女' , 87)
GO
SELECT * FROM Students
