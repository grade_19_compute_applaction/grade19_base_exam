﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    class DBHepler
    {
        //连接数据库
        private static readonly string connStr = "server=.;database=Schools;uid=sa;pwd=123456";
        public static DataTable GetDataTable(string sql)
        {
            //声明数据库对象
            SqlConnection conn = new SqlConnection(connStr);

            //new一个sda指令对象连接sql和conn
            SqlDataAdapter sda = new SqlDataAdapter(sql, conn);

            //打开连接
            conn.Open();

            //获取数据
            DataTable dataTable = new DataTable();
            sda.Fill(dataTable);
            return dataTable;
        }
        public static int AddOrUpdateOrDelete(string sql)
        {
            //声明数据库对象
            SqlConnection conn = new SqlConnection(connStr);

            //new一个cmd指令对象连接sql和conn
            SqlCommand cmd = new SqlCommand(sql, conn);

            //打开连接
            conn.Open();

            //获取数据
            return cmd.ExecuteNonQuery();
        }
    }
}
