﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private int Id;
        public Form2(int Id, string Name, int Age, int Score)
        {
            InitializeComponent();

            this.Id = Id;
            textBox1.Text = Name;
            textBox2.Text = Age.ToString();
            textBox3.Text = Score.ToString();
        }
        //确认添加
        private void button1_Click(object sender, EventArgs e)
        {
            var name = textBox1.Text;
            var age = textBox2.Text;
            var score = textBox3.Text;

            //更新
            if (this.Id > 0)
            {
                var sql = string.Format("update Students set Name = '{0}',Age = {1} ,Score = {2} where Id = {3}", name, age, score, this.Id);
                DBHepler.AddOrUpdateOrDelete(sql);
                MessageBox.Show("更新成功！", "提示");
            }
            //添加
            else
            {
                var sql = string.Format("insert into Students (Name,Age,Score) values ('{0}',{1},{2})", name, age, score);
                DBHepler.AddOrUpdateOrDelete(sql);
                MessageBox.Show("添加成功！", "提示");
            }

            this.DialogResult = DialogResult.Yes;
            this.Close();
        }
        //取消添加
        private void button2_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }
    }
}
