﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: 这行代码将数据加载到表“schoolsDataSet.Students”中。您可以根据需要移动或删除它。
            var stuSql = "select * from Students";

            var dt = DBHelper.GetDataTable(stuSql);

            dataGridView1.DataSource = dt;

            //限制 dataGridView1的某些行为
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView1.ReadOnly = true;
            dataGridView1.AllowUserToAddRows = false;


        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        //查询
        private void button1_Click(object sender, EventArgs e)
        {
            var name = textBox1.Text;
            var sql = string.Format("select * from Students where Name like'%{0}%'", name);
            var dt = DBHelper.GetDataTable(sql);
            dataGridView1.DataSource = dt;
        }

        //添加
        private void button2_Click(object sender, EventArgs e)
        {
            Form2 from = new Form2();
            var res = from.ShowDialog();
            if (res == DialogResult.Yes)
            {
                var stuSql = "select * from Students";

                var dt = DBHelper.GetDataTable(stuSql);

                dataGridView1.DataSource = dt;
            }
            else
            {
                MessageBox.Show("No");
            }
        }

       //更新
        private void button3_Click(object sender, EventArgs e)
        {
            var id = (int)dataGridView1.SelectedRows[0].Cells[0].Value;
            var name = (string)dataGridView1.SelectedRows[0].Cells[1].Value;
            var age = (int)dataGridView1.SelectedRows[0].Cells[2].Value;
            var score = (int)dataGridView1.SelectedRows[0].Cells[3].Value;
            Form2 from = new Form2(id, name, age, score);
            var res = from.ShowDialog();
            if (res == DialogResult.Yes)
            {
                var stuSql = "select * from Students";

                var dt = DBHelper.GetDataTable(stuSql);

                dataGridView1.DataSource = dt;
            }
            else
            {
                MessageBox.Show("No");
            }
        }

       //删除
        private void button4_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                var id = (int)dataGridView1.SelectedRows[0].Cells[0].Value;

                var sql = string.Format("delete from Students where Id={0}", id);

                var res = DBHelper.AddOrUpdateOrDelete(sql);
                if (res == 1)
                {
                    MessageBox.Show("删除成功！", "提示");

                    var stuSql = "select * from Students";

                    var dt = DBHelper.GetDataTable(stuSql);

                    dataGridView1.DataSource = dt;
                }
                else
                {
                    MessageBox.Show("删除失败！", "提示");
                }
            }
            else
            {
                MessageBox.Show("当前未选择需要删除的数据", "提示");
            }
        }
    }
}
