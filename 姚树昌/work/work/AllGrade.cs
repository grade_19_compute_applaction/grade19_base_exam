﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;


namespace work
{
    public partial class AllGrade : Form
    {
        public AllGrade()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            refresh();

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int CIndex = e.ColumnIndex;
            if (CIndex == 0)
            {

                string key1 = this.dataGridView1.CurrentRow.Cells["Column1"].Value.ToString();
                string key2 = this.dataGridView1.CurrentRow.Cells["Column2"].Value.ToString();
                string key3 = this.dataGridView1.CurrentRow.Cells["Column3"].Value.ToString();
                string key4 = this.dataGridView1.CurrentRow.Cells["Column4"].Value.ToString();



                 EditGarde form = new EditGarde();
                form.setKey1(key1);
                form.setKey2(key2);
                form.setKey3(key3);
                form.setKey4(key4);
                form.Owner = this;
                form.Show();
            }
            if (CIndex == 1)
            {
                string key1 = this.dataGridView1.CurrentRow.Cells["Column1"].Value.ToString();

                if (MessageBox.Show("确认删除？", "此删除不可恢复", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    SqlConnection conn = new SqlConnection();
                    conn.ConnectionString = "server=.;database=Schools;user id=sa;password=123";
                    conn.Open();
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandText = "delete from  Students where id = '" + key1 + "'";
                    cmd.Connection = conn;
                    cmd.ExecuteNonQuery();
                    conn.Close();
                    MessageBox.Show("删除成功");
                    refresh();
                }
                
                




            }
        }

        public void refresh()
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = "server=.;database=Schools;user id=sa;password=123";
            conn.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "select * from Students ";
            BindingSource bs = new BindingSource();

            cmd.Connection = conn;
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            sda.Fill(ds);
            this.dataGridView1.DataSource = ds.Tables[0];
            this.dataGridView1.AutoGenerateColumns = true;
            for (int i = 1; i < this.dataGridView1.ColumnCount; i++)
            {
                this.dataGridView1.Columns[i].DefaultCellStyle.SelectionBackColor = Color.White;
                this.dataGridView1.Columns[i].DefaultCellStyle.SelectionForeColor = Color.Black;
                this.dataGridView1.Columns[i].ReadOnly = true;
            }
            this.dataGridView1.AllowUserToAddRows = false;
            conn.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            AddGarde frm = new AddGarde();
            frm.Owner = this;
            frm.Show();
        }
    }
}
