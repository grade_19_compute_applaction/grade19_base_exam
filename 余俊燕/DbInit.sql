create database Schools

 go

 use Schools
 create table Students
 (
	Id int identity primary key, 
	Name nvarchar(80) not null,
	Age int not null,
	Score int not null
 )
 go

 insert into Students (Name,Age,Score)values ('张三',16,88)
 insert into Students (Name,Age,Score)values ('小明',18,77)
 insert into Students (Name,Age,Score)values ('李四',16,96)
 insert into Students (Name,Age,Score)values ('小明',20,65)
 insert into Students (Name,Age,Score)values ('品如',17,99)
 insert into Students(Name,Age,Score)values ('李红',18,45)
 insert into Students(Name,Age,Score)values ('小林',17,86)
 insert into Students (Name,Age,Score)values ('小张',17,66)
 go
  select * from Students
