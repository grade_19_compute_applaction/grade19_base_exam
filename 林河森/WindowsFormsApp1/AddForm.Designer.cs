﻿namespace WindowsFormsApp1
{
    partial class AddForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CancelPreserve = new System.Windows.Forms.Button();
            this.DefinitePreserve = new System.Windows.Forms.Button();
            this.AddScoreBox = new System.Windows.Forms.TextBox();
            this.AddAgeBox = new System.Windows.Forms.TextBox();
            this.AddNameBox = new System.Windows.Forms.TextBox();
            this.AddScore = new System.Windows.Forms.Label();
            this.AddAge = new System.Windows.Forms.Label();
            this.AddName = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // CancelPreserve
            // 
            this.CancelPreserve.Font = new System.Drawing.Font("宋体", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.CancelPreserve.Location = new System.Drawing.Point(350, 243);
            this.CancelPreserve.Name = "CancelPreserve";
            this.CancelPreserve.Size = new System.Drawing.Size(137, 46);
            this.CancelPreserve.TabIndex = 30;
            this.CancelPreserve.Text = "取消";
            this.CancelPreserve.UseVisualStyleBackColor = true;
            this.CancelPreserve.Click += new System.EventHandler(this.CancelPreserve_Click);
            // 
            // DefinitePreserve
            // 
            this.DefinitePreserve.Font = new System.Drawing.Font("宋体", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.DefinitePreserve.Location = new System.Drawing.Point(186, 243);
            this.DefinitePreserve.Name = "DefinitePreserve";
            this.DefinitePreserve.Size = new System.Drawing.Size(137, 46);
            this.DefinitePreserve.TabIndex = 31;
            this.DefinitePreserve.Text = "确定保存";
            this.DefinitePreserve.UseVisualStyleBackColor = true;
            this.DefinitePreserve.Click += new System.EventHandler(this.DefinitePreserve_Click);
            // 
            // AddScoreBox
            // 
            this.AddScoreBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.AddScoreBox.Font = new System.Drawing.Font("宋体", 21.75F);
            this.AddScoreBox.Location = new System.Drawing.Point(283, 170);
            this.AddScoreBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.AddScoreBox.Name = "AddScoreBox";
            this.AddScoreBox.Size = new System.Drawing.Size(204, 34);
            this.AddScoreBox.TabIndex = 25;
            // 
            // AddAgeBox
            // 
            this.AddAgeBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.AddAgeBox.Font = new System.Drawing.Font("宋体", 21.75F);
            this.AddAgeBox.Location = new System.Drawing.Point(283, 126);
            this.AddAgeBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.AddAgeBox.Name = "AddAgeBox";
            this.AddAgeBox.Size = new System.Drawing.Size(204, 34);
            this.AddAgeBox.TabIndex = 26;
            // 
            // AddNameBox
            // 
            this.AddNameBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.AddNameBox.Font = new System.Drawing.Font("宋体", 21.75F);
            this.AddNameBox.Location = new System.Drawing.Point(283, 71);
            this.AddNameBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.AddNameBox.Name = "AddNameBox";
            this.AddNameBox.Size = new System.Drawing.Size(204, 34);
            this.AddNameBox.TabIndex = 24;
            // 
            // AddScore
            // 
            this.AddScore.AutoSize = true;
            this.AddScore.BackColor = System.Drawing.Color.Transparent;
            this.AddScore.Font = new System.Drawing.Font("宋体", 26.25F, System.Drawing.FontStyle.Bold);
            this.AddScore.Location = new System.Drawing.Point(180, 170);
            this.AddScore.Name = "AddScore";
            this.AddScore.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.AddScore.Size = new System.Drawing.Size(106, 35);
            this.AddScore.TabIndex = 27;
            this.AddScore.Text = "成绩:";
            this.AddScore.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // AddAge
            // 
            this.AddAge.AutoSize = true;
            this.AddAge.BackColor = System.Drawing.Color.Transparent;
            this.AddAge.Font = new System.Drawing.Font("宋体", 26.25F, System.Drawing.FontStyle.Bold);
            this.AddAge.Location = new System.Drawing.Point(180, 121);
            this.AddAge.Name = "AddAge";
            this.AddAge.Size = new System.Drawing.Size(106, 35);
            this.AddAge.TabIndex = 28;
            this.AddAge.Text = "年龄:";
            this.AddAge.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // AddName
            // 
            this.AddName.AutoSize = true;
            this.AddName.BackColor = System.Drawing.Color.Transparent;
            this.AddName.Font = new System.Drawing.Font("宋体", 26.25F, System.Drawing.FontStyle.Bold);
            this.AddName.Location = new System.Drawing.Point(180, 70);
            this.AddName.Name = "AddName";
            this.AddName.Size = new System.Drawing.Size(106, 35);
            this.AddName.TabIndex = 29;
            this.AddName.Text = "姓名:";
            this.AddName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // AddForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 362);
            this.Controls.Add(this.CancelPreserve);
            this.Controls.Add(this.DefinitePreserve);
            this.Controls.Add(this.AddScoreBox);
            this.Controls.Add(this.AddAgeBox);
            this.Controls.Add(this.AddNameBox);
            this.Controls.Add(this.AddScore);
            this.Controls.Add(this.AddAge);
            this.Controls.Add(this.AddName);
            this.Name = "AddForm";
            this.Text = "AddForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button CancelPreserve;
        private System.Windows.Forms.Button DefinitePreserve;
        private System.Windows.Forms.TextBox AddScoreBox;
        private System.Windows.Forms.TextBox AddAgeBox;
        private System.Windows.Forms.TextBox AddNameBox;
        private System.Windows.Forms.Label AddScore;
        private System.Windows.Forms.Label AddAge;
        private System.Windows.Forms.Label AddName;
    }
}