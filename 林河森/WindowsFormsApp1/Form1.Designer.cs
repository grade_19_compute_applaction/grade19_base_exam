﻿namespace WindowsFormsApp1
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.UserLogin = new System.Windows.Forms.Button();
            this.UserRegister = new System.Windows.Forms.Button();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.PassWord = new System.Windows.Forms.Label();
            this.UserName = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // UserLogin
            // 
            this.UserLogin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(105)))), ((int)(((byte)(172)))));
            this.UserLogin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.UserLogin.Font = new System.Drawing.Font("宋体", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.UserLogin.ForeColor = System.Drawing.SystemColors.ControlText;
            this.UserLogin.Location = new System.Drawing.Point(179, 199);
            this.UserLogin.Name = "UserLogin";
            this.UserLogin.Size = new System.Drawing.Size(118, 46);
            this.UserLogin.TabIndex = 13;
            this.UserLogin.Text = "登录";
            this.UserLogin.UseVisualStyleBackColor = false;
            this.UserLogin.Click += new System.EventHandler(this.UserLogin_Click);
            // 
            // UserRegister
            // 
            this.UserRegister.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(105)))), ((int)(((byte)(172)))));
            this.UserRegister.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.UserRegister.Font = new System.Drawing.Font("宋体", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.UserRegister.Location = new System.Drawing.Point(357, 199);
            this.UserRegister.Name = "UserRegister";
            this.UserRegister.Size = new System.Drawing.Size(117, 46);
            this.UserRegister.TabIndex = 14;
            this.UserRegister.Text = "注册";
            this.UserRegister.UseVisualStyleBackColor = false;
            this.UserRegister.Click += new System.EventHandler(this.UserRegister_Click);
            // 
            // textBox2
            // 
            this.textBox2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox2.Font = new System.Drawing.Font("宋体", 21.75F);
            this.textBox2.Location = new System.Drawing.Point(321, 139);
            this.textBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(174, 34);
            this.textBox2.TabIndex = 12;
            // 
            // textBox1
            // 
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox1.Font = new System.Drawing.Font("宋体", 21.75F);
            this.textBox1.Location = new System.Drawing.Point(321, 92);
            this.textBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(174, 34);
            this.textBox1.TabIndex = 11;
            // 
            // PassWord
            // 
            this.PassWord.AutoSize = true;
            this.PassWord.BackColor = System.Drawing.Color.Transparent;
            this.PassWord.Font = new System.Drawing.Font("宋体", 26.25F, System.Drawing.FontStyle.Bold);
            this.PassWord.Location = new System.Drawing.Point(173, 138);
            this.PassWord.Name = "PassWord";
            this.PassWord.Size = new System.Drawing.Size(144, 35);
            this.PassWord.TabIndex = 15;
            this.PassWord.Text = "密  码:";
            this.PassWord.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // UserName
            // 
            this.UserName.AutoSize = true;
            this.UserName.BackColor = System.Drawing.Color.Transparent;
            this.UserName.Font = new System.Drawing.Font("宋体", 26.25F, System.Drawing.FontStyle.Bold);
            this.UserName.Location = new System.Drawing.Point(173, 91);
            this.UserName.Name = "UserName";
            this.UserName.Size = new System.Drawing.Size(142, 35);
            this.UserName.TabIndex = 16;
            this.UserName.Text = "用户名:";
            this.UserName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(684, 362);
            this.Controls.Add(this.UserLogin);
            this.Controls.Add(this.UserRegister);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.PassWord);
            this.Controls.Add(this.UserName);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button UserLogin;
        private System.Windows.Forms.Button UserRegister;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label PassWord;
        private System.Windows.Forms.Label UserName;
    }
}

