﻿namespace WindowsFormsApp1
{
    partial class RegisterForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCancelRegister = new System.Windows.Forms.Button();
            this.btnRegister = new System.Windows.Forms.Button();
            this.reTryPassWord = new System.Windows.Forms.TextBox();
            this.TryPassWord = new System.Windows.Forms.TextBox();
            this.TryUserName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.PassWord = new System.Windows.Forms.Label();
            this.user = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnCancelRegister
            // 
            this.btnCancelRegister.Font = new System.Drawing.Font("宋体", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnCancelRegister.Location = new System.Drawing.Point(333, 245);
            this.btnCancelRegister.Name = "btnCancelRegister";
            this.btnCancelRegister.Size = new System.Drawing.Size(137, 46);
            this.btnCancelRegister.TabIndex = 22;
            this.btnCancelRegister.Text = "取消注册";
            this.btnCancelRegister.UseVisualStyleBackColor = true;
            this.btnCancelRegister.Click += new System.EventHandler(this.btnCancelRegister_Click);
            // 
            // btnRegister
            // 
            this.btnRegister.Font = new System.Drawing.Font("宋体", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnRegister.Location = new System.Drawing.Point(192, 245);
            this.btnRegister.Name = "btnRegister";
            this.btnRegister.Size = new System.Drawing.Size(137, 46);
            this.btnRegister.TabIndex = 23;
            this.btnRegister.Text = "注册";
            this.btnRegister.UseVisualStyleBackColor = true;
            this.btnRegister.Click += new System.EventHandler(this.btnRegister_Click);
            // 
            // reTryPassWord
            // 
            this.reTryPassWord.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.reTryPassWord.Font = new System.Drawing.Font("宋体", 21.75F);
            this.reTryPassWord.Location = new System.Drawing.Point(333, 173);
            this.reTryPassWord.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.reTryPassWord.Name = "reTryPassWord";
            this.reTryPassWord.Size = new System.Drawing.Size(204, 34);
            this.reTryPassWord.TabIndex = 17;
            // 
            // TryPassWord
            // 
            this.TryPassWord.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TryPassWord.Font = new System.Drawing.Font("宋体", 21.75F);
            this.TryPassWord.Location = new System.Drawing.Point(333, 124);
            this.TryPassWord.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.TryPassWord.Name = "TryPassWord";
            this.TryPassWord.Size = new System.Drawing.Size(204, 34);
            this.TryPassWord.TabIndex = 18;
            // 
            // TryUserName
            // 
            this.TryUserName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TryUserName.Font = new System.Drawing.Font("宋体", 21.75F);
            this.TryUserName.Location = new System.Drawing.Point(333, 77);
            this.TryUserName.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.TryUserName.Name = "TryUserName";
            this.TryUserName.Size = new System.Drawing.Size(204, 34);
            this.TryUserName.TabIndex = 16;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("宋体", 26.25F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(151, 172);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(178, 35);
            this.label1.TabIndex = 19;
            this.label1.Text = "确认密码:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // PassWord
            // 
            this.PassWord.AutoSize = true;
            this.PassWord.BackColor = System.Drawing.Color.Transparent;
            this.PassWord.Font = new System.Drawing.Font("宋体", 26.25F, System.Drawing.FontStyle.Bold);
            this.PassWord.Location = new System.Drawing.Point(147, 124);
            this.PassWord.Name = "PassWord";
            this.PassWord.Size = new System.Drawing.Size(182, 35);
            this.PassWord.TabIndex = 20;
            this.PassWord.Text = "密    码:";
            this.PassWord.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // user
            // 
            this.user.AutoSize = true;
            this.user.BackColor = System.Drawing.Color.Transparent;
            this.user.Font = new System.Drawing.Font("宋体", 26.25F, System.Drawing.FontStyle.Bold);
            this.user.Location = new System.Drawing.Point(149, 72);
            this.user.Name = "user";
            this.user.Size = new System.Drawing.Size(180, 35);
            this.user.TabIndex = 21;
            this.user.Text = "用 户 名:";
            this.user.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // RegisterForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 362);
            this.Controls.Add(this.btnCancelRegister);
            this.Controls.Add(this.btnRegister);
            this.Controls.Add(this.reTryPassWord);
            this.Controls.Add(this.TryPassWord);
            this.Controls.Add(this.TryUserName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.PassWord);
            this.Controls.Add(this.user);
            this.Name = "RegisterForm";
            this.Text = "RegisterForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCancelRegister;
        private System.Windows.Forms.Button btnRegister;
        private System.Windows.Forms.TextBox reTryPassWord;
        private System.Windows.Forms.TextBox TryPassWord;
        private System.Windows.Forms.TextBox TryUserName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label PassWord;
        private System.Windows.Forms.Label user;
    }
}