﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class EidForm2 : Form
    {
        public EidForm2()
        {
            InitializeComponent();
        }

        public EidForm2()
        {
            InitializeComponent();
        }
        private int id;
        
        public EidForm2(int id, string name, int age, int score)
        {
            InitializeComponent();

            this.id = id;
            textBox1.Text = name;
            textBox2.Text = age.ToString();
            textBox3.Text = score.ToString();

            
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            SqlCommand command = new SqlCommand();

            command.ExecuteNonQuery();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var name = textBox1.Text;
            var age = textBox2.Text;
            var score = textBox3.Text;
            if (this.id > 0)
            {
                var sql = string.Format("update student set studentName='{0}',Age={1},Score={2} where Id={3}", name, age, score, this.id);

                DbHelper.AddOrUpdateOrDelete(sql);

                MessageBox.Show("更新成功！", "提示");
            }

            else
            {
                var sql = string.Format("insert into student (StuName,Age,Score) values('{0}',{1},{2})", name, age, score);

                DbHelper.AddOrUpdateOrDelete(sql);

                MessageBox.Show("添加成功!", "提示");
            }

            this.DialogResult = DialogResult.Yes;

            this.Close();
        }
    }
}
