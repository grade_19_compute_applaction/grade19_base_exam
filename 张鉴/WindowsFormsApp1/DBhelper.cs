﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    public class DBhelper
    {
        private readonly static string coonString = "server=.;database = Schools;uid=sa;pwd=123456";
        public static DataTable GetDataTable(string sql)
        {
            SqlConnection connection = new SqlConnection(coonString);
            SqlDataAdapter adapter = new SqlDataAdapter(sql, connection);
            connection.Open();
            DataTable data = new DataTable();
            adapter.Fill(data);
            return data;

        }
        public static int AddorUpdateorDelect(string sql)
        {
            SqlConnection connection = new SqlConnection(coonString);
            SqlCommand command = new SqlCommand(sql, connection);
            connection.Open();
            return command.ExecuteNonQuery();
        }

    }
}
