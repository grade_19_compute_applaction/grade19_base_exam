create database Schools
go
use Schools
create table Students
(
 id int primary key identity(1,1),
 sname nvarchar(10) not null,
 sage int default(18),
 sex nchar(2) default('男'),
 score float not null 
)
insert into Students(sname,sage,sex,score) values
 ('段霄',20,'女',80),
 ('陈君何',18,'男',85),
 ('李进步',20,'女',90),
 ('李青桐',20,'女',70),
 ('范胖',19,'女',60),
 ('吴朝阳',20,'女',60),
 ('严良',18,'女',70),
 ('普普',19,'女',65)
 select * from Students where sname like '%%';
 update Schools set sname='',sage='',sex='',score='';