create database Schools

Go

use Schools

Go

create table Students
(
	Id int primary key identity(1,1) not null,
	Name nvarchar (10) not null,
	Age int not null,
	Score int default 0 not null,
)

insert into Students (Name,Age,Score) values ('���׷�',20,95),
											 ('�׾�ͤ',19,86),
											 ('��һ��',19,92),
											 ('�����Ȱ�',19,83),
											 ('��¶˼',19,75),
											 ('�����',18,87),
											 ('����',20,74),
											 ('����',20,68)