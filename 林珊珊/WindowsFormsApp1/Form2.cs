﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form2 : Form
    { 
        public Form2()
        {
            InitializeComponent();      
        }
        private int id;

        public Form2(int id, string name, int age, int score)
        {
            InitializeComponent();

            this.id = id;
            textBox1.Text = name;
            textBox2.Text = age.ToString();
            textBox3.Text = score.ToString();
        }
        private void Form2_Load(object sender, EventArgs e)
        {

        }
        //确认保存
        private void button1_Click(object sender, EventArgs e)
        {
            var name = textBox1.Text;
            var age = textBox2.Text;
            var score = textBox3.Text;

            //更新
            if (this.id > 0)
            {
                var sql = string.Format("update Students set name = '{0}',age = '{1}',score = '{2}'where id = '{3}'", name, age, score, this.id);
                DBHelper.Add(sql);
                MessageBox.Show("更新成功！", "提示");
            }
            else
            {
                var sql = string.Format("insert into Students (name,age,score) values ('{0}','{1}','{2}')", name, age, score);
                DBHelper.Add(sql);
                MessageBox.Show("保存成功！", "提示");
            }
            this.DialogResult = DialogResult.Yes;
            this.Close();
        }
        //取消保存
        private void button2_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }
    }
}
