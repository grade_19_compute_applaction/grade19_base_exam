﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    public class DBHelper
    {
        //连接数据库
        private static string connStr = "server=DESKTOP-512404B;database=Schools;uid=sa;pwd=123456";
        public static DataTable GetDataTable(string sql)
        {

            SqlConnection conn = new SqlConnection(connStr);

            SqlDataAdapter sda = new SqlDataAdapter(sql, conn);

            conn.Open();

            DataTable dataTable = new DataTable();
            sda.Fill(dataTable);
            return dataTable;
        }

        public static int Add(string sql)
        {

            SqlConnection conn = new SqlConnection(connStr);

            SqlCommand cmd = new SqlCommand(sql, conn);

            conn.Open();

            return cmd.ExecuteNonQuery();
        }
    }
}
