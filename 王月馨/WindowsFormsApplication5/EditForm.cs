﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication5
{
    public partial class EditForm : Form
    {
        public EditForm()
        {
            InitializeComponent();
        }

        private int id;

        public EditForm(int id, string name, int age, int score )
        {
            // TODO: Complete member initialization
            InitializeComponent();
            this.id = id;
            textBox1.Text = id.ToString();
            textBox2.Text = name;
            textBox3.Text = age.ToString();
            textBox4.Text = score.ToString();
        }
       
        /// <summary>
        /// 年龄
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void label3_Click(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// 学号ID
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void label1_Click(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// 姓名;
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void label2_Click(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// 分数
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void label4_Click(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// 取消
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;

            this.Close();

        }
        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            var id = textBox1.Text;

            var name = textBox2.Text;

            var age = textBox3.Text;

            var score = textBox4.Text;
            //修改
            if (this.id > 0)
            {
                var sql = string.Format("update Students set StudID='{0}',StuName='{1}',StuAge='[2]',StuScore='[3]'", id , name , age  , score );

                DBHepler.AddDrUpdateOrDelete(sql);

                MessageBox.Show("添加成功！", "消息提示框");
            }
            else 
            {
                var sql = string.Format("insert into Students (StuID,StuName,StuAge,StuScore) values ('{0}','{1}','{2}','{3}')",id  ,name  ,age  ,score  );

                DBHepler.AddDrUpdateOrDelete(sql);

                MessageBox.Show("添加成功！", "消息提示框");

            }
            this.DialogResult = DialogResult .Yes ;

            this .Close ();
        }
    }
}
