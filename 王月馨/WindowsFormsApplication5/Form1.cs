﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication5
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            var Stu = "select * from Students";

            var dt = DBHepler.GetDataTable(Stu);

            dataGridView1.DataSource = dt;


            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            dataGridView1.ReadOnly = true;

            dataGridView1.AllowUserToAddRows = false;
        }

        /// <summary>
        /// 姓名：
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void label1_Click(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            var name = textBox1.Text.Trim();

            var sql = string.Format("select * from Students where StuName like '%{0}%'", name);

            var dt = DBHepler.GetDataTable(sql);

            dataGridView1.DataSource = dt;

        }
        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            EditForm ef = new EditForm();

            var res = ef.ShowDialog();

            if (res == DialogResult.Yes)
            {

                var Stu = "select * from Students";

                var dt = DBHepler.GetDataTable(Stu);

                dataGridView1.DataSource = dt;
            }
            else 
            {

                MessageBox.Show("No");
            }
        }
        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {

                var id = (int)dataGridView1.SelectedRows[0].Cells["StuID"].Value;

                var name = (string)dataGridView1.SelectedRows[0].Cells["StuName"].Value;

                var age = (int)dataGridView1.SelectedRows[0].Cells["StuAge"].Value;

                var score = (int)dataGridView1.SelectedRows[0].Cells["StuScore"].Value;

                EditForm ef = new EditForm(id, name , age , score );

                var res = ef.ShowDialog();

                if (res == DialogResult.Yes)
                {
                    var Stu = "select * from Students";

                    var dt = DBHepler.GetDataTable(Stu);

                    dataGridView1.DataSource = dt;
                }
                else
                {
                    MessageBox.Show("No");
                }
            }
            else 
            {
                MessageBox.Show("当前未选择更新的数据！", "消息提示框");
            }
        }
        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button4_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                var id = (int)dataGridView1.SelectedRows[0].Cells["StuID"].Value;

                var sql = string.Format("delete from Students where StuID={0}", id);

                var res = DBHepler.AddDrUpdateOrDelete(sql);
                if (res == 1)
                {
                    MessageBox.Show("删除成功！", "消息提示框");

                    var abc = "select * from Students";

                    var dt = DBHepler.GetDataTable(abc);

                    dataGridView1.DataSource = dt;
                }
                else
                {
                    MessageBox.Show("删除失败！", "消息提示框");
                }
            }
            else
            {
                MessageBox.Show("当前未选中需要删除的数据！", "消息提示框");
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

      
    }
}
