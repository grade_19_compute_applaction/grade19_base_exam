--创建Schools数据库
CREATE DATABASE Schools 
 ON PRIMARY 
 (
 NAME='Schools_data',
 FILENAME='D:\我的学习\Schools_data.mdf',
 SIZE=5MB,
 FILEGROWTH=10%
 )
 LOG ON
  (
  NAME='Schools_log',
 FILENAME='D:\我的学习\Schools_log.ldf',
 SIZE=50MB,
 FILEGROWTH=1MB
  )
  GO
  
  --创建表
  USE Schools 
   GO 
   CREATE TABLE Students
   (
   StuID int  NOT NULL,
   StuName nvarchar(40) NOT NULL,
   StuAge int NOT NULL,
   StuScore int NOT NULL, 
   )
   GO

   --增加表格中的数据
   
   INSERT INTO Students (StuID ,StuName,StuAge ,StuScore ) VALUES(19001,'晓红',20,85); 
   INSERT INTO Students (StuID,StuName,StuAge,StuScore)  VALUES (19002,'全贵兰',21,84);
   INSERT INTO Students (StuID,StuName,StuAge,StuScore)  VALUES (19003,'王子晴',22,85);
   INSERT INTO Students (StuID,StuName,StuAge,StuScore)  VALUES (19004,'夏雪若',18,80)
   INSERT INTO Students (StuID,StuName,StuAge,StuScore)  VALUES (19005,'小明',5,75)
   INSERT INTO Students (StuID,StuName,StuAge,StuScore)  VALUES (19006,'黄磊',55,69)
   INSERT INTO Students (StuID,StuName,StuAge,StuScore)  VALUES (19007,'王青',19,59)
   INSERT INTO Students (StuID,StuName,StuAge,StuScore)  VALUES (19008,'王一博',23,90)
   INSERT INTO Students (StuID,StuName,StuAge,StuScore)  VALUES (19009,'刘亦菲',43,93)
   INSERT INTO Students (StuID,StuName,StuAge,StuScore)  VALUES (19010,'杨幂',38,98)
   INSERT INTO Students (StuID,StuName,StuAge,StuScore)  VALUES (19011,'李易峰',36,88)
   INSERT INTO Students (StuID,StuName,StuAge,StuScore)  VALUES (19012,'迪丽热巴',26,76)
   INSERT INTO Students (StuID,StuName,StuAge,StuScore)  VALUES (19013,'马天宇',29,55)
   INSERT INTO Students (StuID,StuName,StuAge,StuScore)  VALUES (19014,'李钟硕',30,44)
   INSERT INTO Students (StuID,StuName,StuAge,StuScore)  VALUES (19015,'肖战',27,59)
   
   --删除表中多余数据
   -- DELETE FROM Srudents   where StudID='19006'
	--查看表中数据
   select * from Students
     
	 select * from Students where StuName like '%王%';