﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WinformTest;

namespace WinformTest2
{
    public partial class Editform : Form
    {
        public Editform()
        {
            InitializeComponent();
        }

        public Editform(int id, string name,int age,int score)
        {
            InitializeComponent();
            this.id = id;
            textBox1.Text = name;
            textBox2.Text = age.ToString();
            textBox3.Text = score.ToString();
        }

        private int id;
        private void Editform_Load(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// 确认保存
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        { var ageString = textBox2.Text.ToString();
            var scoreString = textBox3.Text.ToString();
            var name = textBox1.Text;
            var age = ageString.Count() > 0 ? int.Parse(ageString) : 18;
            var score = scoreString.Count() > 0 ? int.Parse(scoreString) : 0;
            //更新
            if (id > 0)
            {
                if (!string.IsNullOrEmpty(name))
                {
                    var sql = string.Format("update Students set Name='{0}',Age={1},Score={2}where Id={3}", name,age,score, this.id);
                    var res = DbHelp.AddOrEditSave(sql);
                    if (res == 1)
                    {
                        MessageBox.Show("修改成功！", "提示");
                        this.DialogResult = DialogResult.Yes;
                        this.Close();
                    }
                }
            }
            //新增
            else
            {




                if (!string.IsNullOrEmpty(name))
                {
                    var sql = string.Format("insert into Students(Name,Age,Score)values('{0}',{1},{2})", name,age,score);
                    var res = DbHelp.AddOrEditSave(sql);
                    if (res == 1)
                    {
                        MessageBox.Show("添加成功！", "提示");
                        this.DialogResult = DialogResult.Yes;
                        this.Close();
                    }
                    //else
                    //{
                    //    MessageBox.Show("添加失败！", "提示");
                    //    this.Close();
                    //}
                }
                else
                {
                    MessageBox.Show("学生姓名不能为空！", "提示");

                }
            }

        }
        /// <summary>
        /// 取消
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }
    
    }
}
