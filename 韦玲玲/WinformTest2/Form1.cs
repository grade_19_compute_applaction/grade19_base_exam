﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WinformTest;

namespace WinformTest2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            var sql = "select * from Students";
            var dt = DbHelp.GetDataTable(sql);
            dataGridView1.DataSource = dt;
            dataGridView1.ReadOnly = true;
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView1.AllowUserToAddRows = false;
        }
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            var name = textBox1.Text;
            var sql = string.Format("select * from Students where Name like '%{0}%'", name);
            var dt = DbHelp.GetDataTable(sql);
            dataGridView1.DataSource = dt;
        }
        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                var row = dataGridView1.SelectedRows[0];
                var id = row.Cells["Id"].Value;
                var res = MessageBox.Show("你确定要删除吗？", "提示", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                if (res.Equals(DialogResult.Yes))
                {
                    var sql = string.Format("delete from Students where Id={0}", id);
                    var rowCount = DbHelp.AddOrEditSave(sql);
                    if (rowCount > 0)
                    {
                        var sqlStu = "select * from Students";
                        var dt = DbHelp.GetDataTable(sqlStu);
                        dataGridView1.DataSource = dt;
                        MessageBox.Show("删除成功", "提示");
                    }
                    else
                    {
                        MessageBox.Show("没有选择任何的行", "提示");
                    }
                }
            }
        }
        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count>0)
            {
                var id = Convert.ToInt32(dataGridView1.SelectedRows[0].Cells["Id"].Value);
                var name = (string)dataGridView1.SelectedRows[0].Cells["Name"].Value;
                var age = (int)dataGridView1.SelectedRows[0].Cells["Age"].Value;
                var score = (int)dataGridView1.SelectedRows[0].Cells["Score"].Value;
                Editform editform = new Editform(id,name,age,score);
               var res= editform.ShowDialog();
                if (res == DialogResult.Yes)
                {       //更新当前数据
                    var nameTemp = textBox1.Text;
                    var sql = string.Format("select * from Students where Name like '%{0}%'", nameTemp);
                    var dt = DbHelp.GetDataTable(sql);
                    dataGridView1.DataSource = dt;
                }
                else
                {
                    MessageBox.Show("这个是No");
                }


            }
            else
            {
                MessageBox.Show("您未选择任何的数据行", "提示");
            }
        }
        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button4_Click(object sender, EventArgs e)
        {
            Editform editform = new Editform();
            var res =editform.ShowDialog();
            if (res==DialogResult.Yes)
            {       //更新当前数据
                var name = textBox1.Text;
                var sql = string.Format("select * from Students where Name like '%{0}%'", name);
                var dt = DbHelp.GetDataTable(sql);
                dataGridView1.DataSource = dt;
            }
            else
            {
                MessageBox.Show("这个是No");
            }
        }
    }
}
