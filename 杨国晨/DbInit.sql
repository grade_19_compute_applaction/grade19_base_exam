USE MASTER

GO

IF EXISTS(SELECT * FROM SYSDATABASES WHERE NAME ='Schools')
DROP DATABASE Schools

CREATE DATABASE Schools

GO
USE Schools

GO

IF EXISTS(SELECT * FROM SYSOBJECTS WHERE NAME ='Students')
DROP TABLE Students

CREATE TABLE Students
(
   Id int primary key identity(1,1),
   Name nvarchar(20) not null,
   Age int not null,
   Score int not null

)

insert into Students(Name,Age,Score) values('�����',20,99)
insert into Students(Name,Age,Score) values('����',20,98)

select * from Students


