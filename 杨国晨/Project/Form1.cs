﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: 这行代码将数据加载到表“schoolsDataSet.Students”中。您可以根据需要移动或删除它。
            var sql = "select * from Students";

            var data = DBhelper.GetDataTable(sql);
            dataGridView1.DataSource = data;

            dataGridView1.ReadOnly = true;

            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect ;

            dataGridView1.AllowUserToAddRows = false;
        }
        /// <summary>
        /// 模糊查询
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            var name = textBox1.Text;

            var sql = string.Format("select * from Students where Name like '%{0}%' ",name);

            var data = DBhelper.GetDataTable(sql);

            dataGridView1.DataSource = data;
        }
        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            var Id = (int)dataGridView1.SelectedRows[0].Cells[0].Value;

            var Name = (string)dataGridView1.SelectedRows[0].Cells[1].Value;

            var Age = (int)dataGridView1.SelectedRows[0].Cells[2].Value;

            var Score = (int)dataGridView1.SelectedRows[0].Cells[3].Value;

            Form2 form2 = new Form2(Id, Name, Age, Score);


            var res= form2.ShowDialog();

            if (res == DialogResult.Yes)
            {
                var sql = "select * from Students";

                var data = DBhelper.GetDataTable(sql);

                dataGridView1.DataSource = data;
            }
            else
            {
                MessageBox.Show("请进行正确操作！！！", "提示");


            }

        }
        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {
            Form2 form2 = new Form2();

            var res = form2.ShowDialog();

            if (res == DialogResult.Yes)
            {
                var sql = "select * from Students";

                var data = DBhelper.GetDataTable(sql);

                dataGridView1.DataSource = data;
            }
            else
            {
                MessageBox.Show("请进行正确操作！！！", "提示");


            }

        }

        private void button4_Click(object sender, EventArgs e)
        {
            var res = dataGridView1.SelectedRows;
            if (res.Count > 0)
            {
                var Id = (int)res[0].Cells[0].Value;
                var sql = string.Format("delete from Students where Id={0}", Id);

                var resCount = DBhelper.AddOrUpdateOrDelete(sql);

                if (resCount > 0)
                {
                    MessageBox.Show("删除成功!", "提示");
                    var strSql = "select * from Students";
                    var data = DBhelper.GetDataTable(strSql);
                    dataGridView1.DataSource = data;

                }
                else
                {
                    MessageBox.Show("删除失败！", "提示");
                }
            }
            else
            {
                MessageBox.Show("请选择你要删除的行", "提示");

            }
        }
    }
}
