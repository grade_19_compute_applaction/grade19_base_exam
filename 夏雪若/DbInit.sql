USE master 
GO
IF EXISTS (SELECT * FROM SYSDATABASES WHERE NAME='Schools')
DROP DATABASE Schools
GO
CREATE DATABASE Schools
ON PRIMARY
(
	NAME='Schools',
	FILENAME='D:\SQLproject\Schools.mdf',
	SIZE=5MB,
	FILEGROWTH=1MB
)
LOG ON
(
	NAME='Schools_log',
	FILENAME='D:\SQLproject\Schools_log.ldf',
	SIZE=1MB,
	FILEGROWTH=10%
)
GO
USE Schools
GO
IF EXISTS(SELECT * FROM SYSOBJECTS WHERE NAME = 'stuinfo ')
DROP TABLE Students 
GO
CREATE TABLE Students
(
	Id INT IDENTITY(1,1) PRIMARY KEY,
	Name NVARCHAR(10) NOT NULL,
	Age INT DEFAULT 18 NOT NULL,
	Score INT CHECK(Score BETWEEN 0 AND 100) NOT NULL
)
GO

INSERT INTO Students(Name,Age,Score) VALUES ('���� ',22 , 100 )
INSERT INTO Students(Name,Age,Score) VALUES ('½ѩ ',19 , 75 )
INSERT INTO Students(Name,Age,Score) VALUES ('���� ',20 , 93 )
INSERT INTO Students(Name,Age,Score) VALUES ('�˽� ',20 , 95 )
INSERT INTO Students(Name,Age,Score) VALUES ('���� ',21 , 100 )
INSERT INTO Students(Name,Age,Score) VALUES ('Ѧ���� ',20 , 50 )
INSERT INTO Students(Name,Age,Score) VALUES ('������ ',19 , 85 )
INSERT INTO Students(Name,Age,Score) VALUES ('����� ',20, 60 )
INSERT INTO Students(Name,Age,Score) VALUES ('�Գ��� ',19 , 40 )
INSERT INTO Students(Name,Age,Score) VALUES ('���� ',22 , 90 )
INSERT INTO Students(Name,Age,Score) VALUES ('��ǳ�� ',19 , 93 )
INSERT INTO Students(Name,Age,Score) VALUES ('��t ',20 , 88 )

SELECT * FROM Students