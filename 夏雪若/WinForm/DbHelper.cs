﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinForm
{
    public class DbHelper
    {
        public static readonly string constring = "server=.;database=Schools;uid=sa;pwd=sa120420";

        public static DataTable GetDataTable(string sql)
        {
            SqlConnection connection = new SqlConnection(constring);

            SqlDataAdapter adapter = new SqlDataAdapter(sql,connection);

            connection.Open();

            DataTable dt = new DataTable();

            adapter.Fill(dt);

            return dt;
        }
        public static int AddOrUpdateOrDelete(string sql)
        {
            SqlConnection connection = new SqlConnection(constring);

            SqlCommand command = new SqlCommand(sql,connection);

            connection.Open();

            return command.ExecuteNonQuery();
        }
    }
}
