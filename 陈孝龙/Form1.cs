﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            var cxl = "select * from Schools";

            var cxll = "select * from Students";

            var dt = Class1.GetDataTable(cxll);

            dataGridView1.DataSource = dt;

            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView1.ReadOnly = true;
            dataGridView1.AllowUserToAddRows = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var name = textBox1.Text;
            
            var sql = string.Format("select * from Students where Name like'%{0}%'", name);
            
            var dt = Class1.GetDataTable(sql);
            
            dataGridView1.DataSource = dt;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //添加
            Form2 form = new Form2();
            var res = form.ShowDialog();
            if(res == DialogResult.Yes)
            {
                var cxll = "select * from Students";
                var dt = Class1.GetDataTable(cxll);
                dataGridView1.DataSource = dt;
            }
            else
            {
                MessageBox.Show("no");
            }
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //更新
            var id = (int)dataGridView1.SelectedRows[0].Cells["Id"].Value;

            var name = (int)dataGridView1.SelectedRows[0].Cells["name"].Value;

            var age = (int)dataGridView1.SelectedRows[0].Cells["Age"].Value;
            
            var score = (int)dataGridView1.SelectedRows[0].Cells["Score"].Value;

            Form2 form = new Form2(id,Name,age,score);
            var res= form.ShowDialog();
            if (res == DialogResult.Yes)
            {
                var cxll = "select*from Students;";
                var data = Class1.GetDataTable(cxll);
                dataGridView1.DataSource = data;
            }else
            {
                MessageBox.Show("no");
            }
           
        }

        private void button4_Click(object sender, EventArgs e)
        {
            //删除
            var id = dataGridView1.SelectedRows[0].Cells[id].Value;
            var sql = string.Format("delete from Students where id={0},id");
            var res = DBHelper.AddOrUpate(sql);
            if (res == 1)
            {
                MessageBox.Show("删除成功");
                var cxll = "select* from Students";
                var dt = Class1.GetDataTable(cxll);
                dataGridView1.DataSource = dt;

            }
            else
            {
                MessageBox.Show("删除失败");
            }
        }
    }
}
