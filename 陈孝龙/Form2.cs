﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp3
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }
        private int Id;

        public Form2(int id, string name, int age, int score)
        {
            InitializeComponent();
            this.Id = id;
            textBox1.Text = name;
            textBox2.Text = age.ToString();
            textBox3.Text = score.ToString();


        }
        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var name = textBox1.Text;
            var age = textBox2.Text;
            var score = textBox3.Text;
            if (this.Id > 0)
            {
                var sql = string.Format("update Students set Name='{0}',Age={1},score={2}where Id={4}", name, age, score, this.Id);
                DBHelper.AddOrUpateOrDelete(sql);
                MessageBox.Show("更新成功");
            }
            else
            {
                var sql = string.Format("insert into Students(Name,Age,Score)values('{0}',{1},{2})", name, age, score);
                DBHelper.AddOrUpateOrDelete(sql);
                MessageBox.Show("添加成功");

            }
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

    }
}
