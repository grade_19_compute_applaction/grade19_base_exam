
--数据库名
create database Schools

use Schools
create table Students
(
Id int primary key identity ,--
Name nvarchar(255) not null,--
Age int  not null,--
Score int not null,
);

insert into Students(Name,Age,Score)
values('小强',19,90),
	  ('小名',18,80),
	  ('小李',18,80),
	  ('李四',18,80);

select * from Students