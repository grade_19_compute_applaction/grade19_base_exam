create database Schools
go
use Schools
go
create table Students
(Id int primary key identity,
StudentName nvarchar(80) not null,
Age int default 18,
Score int default 0
)
go
insert into Students(StudentName) values('王慧华')
insert into Students(StudentName) values('翠花')
insert into Students(StudentName) values('小鸡儿')
insert into Students(StudentName) values('小杰')
go