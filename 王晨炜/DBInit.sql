create database Schools
go
create table Students
(
	Id int primary key identity(1,1) not null,
	Name nvarchar(10) not null,
	Age int  not null,
	Score int  not null,
)
insert into Students (Name,Age,Score) values ('张三',20,76),('李四',20,95),
											 ('王五',18,100),('李白',19,59),
											 ('王强',19,67),('张璇',19,53),
											 ('张翠花',20,59),('李翠花',19,66),
											 ('黄山河',19,87)
