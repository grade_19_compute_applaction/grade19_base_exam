﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    public class DbHelper
    {
        private static readonly string coStr = "server=.;database=Schools;uid=sa;pwd=123456";
        public static DataTable GetDataTable(string sql)
        {
            SqlConnection con = new SqlConnection(coStr);
            SqlDataAdapter sqlData = new SqlDataAdapter(sql, con);
            con.Open();
            DataTable dt = new DataTable();
            sqlData.Fill(dt);
            return dt;
        }
        public static int AddOrUpdateOrDelete(string sql)
        {
            SqlConnection con = new SqlConnection(coStr);
            SqlCommand com = new SqlCommand(sql, con);
            con.Open();
            var res = com.ExecuteNonQuery();
            return res;

        }
    }
}
