﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: 这行代码将数据加载到表“schoolsDataSet.Students”中。您可以根据需要移动或删除它。
            this.studentsTableAdapter.Fill(this.schoolsDataSet.Students);
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            dataGridView1.ReadOnly = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var name = textBox1.Text;
            var sql = string.Format("select * from Students where Name like '%{0}%'", name);
            var dt = DbHelper.GetDataTable(sql);
            dataGridView1.DataSource = dt;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form2 form2 = new Form2();
            var res = form2.ShowDialog();

            if (res == DialogResult.OK)
            {
                var sql = "select * from Students";
                var dt = DbHelper.GetDataTable(sql);
                dataGridView1.DataSource = dt;
            }
            else
            {
                MessageBox.Show("NO！");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var id = (int)dataGridView1.SelectedRows[0].Cells[0].Value;
            var name = (string)dataGridView1.SelectedRows[0].Cells[1].Value;
            var age = (int)dataGridView1.SelectedRows[0].Cells[2].Value;
            var score = (int)dataGridView1.SelectedRows[0].Cells[3].Value;

            Form2 form2 = new Form2(id, name, age, score);
            var res = form2.ShowDialog();

            if (res == DialogResult.OK)
            {
                var sql = "select * from Students";
                var dt = DbHelper.GetDataTable(sql);
                dataGridView1.DataSource = dt;
            }
            else
            {
                MessageBox.Show("NO！");
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            var id = dataGridView1.SelectedRows[0].Cells[0].Value;
            var sql = string.Format("delete from Students where Id={0}", id);
            var res = DbHelper.AddOrUpdateOrDelete(sql);
            if (res == 1)
            {
                MessageBox.Show("删除成功！", "提示");
                var sqls = "select * from Students";
                var dt = DbHelper.GetDataTable(sqls);
                dataGridView1.DataSource = dt;
            }
            else
            {
                MessageBox.Show("删除失败！", "提示");
            }
        }
    }
}
