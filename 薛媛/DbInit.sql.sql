
/*创建数据库*/
CREATE DATABASE [Schools] ;

/*创建表*/
CREATE TABLE Students(
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nchar](10) NULL,
	[Age] [int] NULL,
	[Score] [int] NULL,
	[ClassNum] [int] NULL,
	[Pwd] [nchar](10) NULL) 

/*插入测试数据*/
insert into Students values('张三',29,90,1,'123')
insert into Students values('李四',26,77,2,'234')
insert into Students values('王五',25,66,3,'345')
insert into Students values('张柳',23,87,2,'3434') 
 
 
