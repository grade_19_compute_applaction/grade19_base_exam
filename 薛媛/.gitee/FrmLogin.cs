﻿using DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FrmMain
{
    public partial class FrmLogin : Form
    {
        FrmMain frmMain = null;
        public FrmLogin()
        {
            InitializeComponent();
            this.txtRePwd.Visible = false;
            this.lblRePwd.Visible = false;
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            string strName = this.txtName.Text.Trim();
            string strPwd = this.txtPwd.Text.Trim();

            if(string.IsNullOrEmpty(strName))
            {
                MessageBox.Show("请输入用户名！");
                return;
            }
            if(string.IsNullOrEmpty(strPwd))
            {
                MessageBox.Show("请输入密码！");
                return;
            }

            if (this.btnLogin.Text.Trim() == "登录")
            {
                string strSql = "select * from Students where Name='" + strName + 
                    "' and Pwd = '" + strPwd + "'";
                DataSet ds = DBHelp.GetDataSet(strSql);
                if(ds.Tables.Count>0)
                {
                    MessageBox.Show("登录成功！");
                    frmMain = new FrmMain(strName);
                    frmMain.Show();
                    this.Hide();
                }
                else
                {
                    MessageBox.Show("用户名或密码错误");
                    return;
                }

            }

            if(this.btnLogin.Text.Trim()=="注册")
            {
                string strSql = "insert into Students values('" + 
                    strName + "',0,0,0,'" + strPwd + "')";
                int intRe = DBHelp.ExecuteNonQuery(strSql);
                if(intRe>0)
                {
                    MessageBox.Show("注册成功！");
                    this.lblLogin.Text = "登录用户";
                    this.btnLogin.Text = "登录";
                    this.txtRePwd.Visible = false;
                    this.lblRePwd.Visible = false;
                }
            }
        }

        private void btnCancle_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void lblReg_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.lblLogin.Text = "注册用户";
            this.btnLogin.Text = "注册";
            this.txtRePwd.Visible = true;
            this.lblRePwd.Visible = true;

        }
    }
}
